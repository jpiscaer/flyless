# Flyless - a Developer Advocacy community

## Description
A virtual community focused on making Developer Relations more sustainable for its practictioners and the world. 

## Discord 
Our community has a Discord Server that is the meeting place for both chat and synchronous calls. Please reach out to a Flyless member if you'd like an invite. If you don't know a Flyless member, open an issue in this project and tag `@johncoghlan` in the issue or a comment. 

## Contributing
We are always looking for community managers to help organize the calls and manage our social media, speakers to help support our monthly calls, and community members to attend and contribute to the calls. If you are interested in any of these roles, please let us know by opening an issue and tagging `@johncoghlan`. 

## Authors and acknowledgment
[Katie Reese](https://twitter.com/katiereese317) and [James Governor](https://twitter.com/monkchips) are the co-founders of Flyless. 

## License
This project uses the MIT License. 

## Project status
We are actively looking for speakers and a community manager. 

***
# Collaborate with GitLab

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/flyless-dev/documentation.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

***

