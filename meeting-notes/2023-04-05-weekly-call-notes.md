# Mission statement and charter 

In order to set the community on a good path forward, we'll need a mission statement and charter to help recruit members and set up future people for success. 

Multiple different centers of gravity in the group, need to align. Can also be an outlet for people to elevate above the busywork. 

Guiding principles: 
- Thoughtful
- Sustainable
- Dynamic

## Flyless

Limiting travel

## DevRel 

Professionalization of DevRel 

## Value we get from this community 

- Shared experience, don't have to do the work to level set before diving into discussions
- Many facets of DevRel represented (geographic, seniority, role, org size, etc)
- Scheduled talks are valuable
- Can be authentic self in this group 

# Potential meeting cadence 

1. Monthly presentation
1. Monthly community call
1. Monthly mastermind/problem solving session 

# Opportunity

Create a community manager role for someone who is entering the industry to lead this group and arrange the calls, manage communications including documentation, and grow the community. 
